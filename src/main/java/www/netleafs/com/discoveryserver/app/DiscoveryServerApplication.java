package www.netleafs.com.discoveryserver.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
// Enable eureka server
@EnableEurekaServer
public class DiscoveryServerApplication extends SpringBootServletInitializer {

    /**
     * An Entry point for any Boot applications
     * <br> Used for JAR creations
     * @param args
     */
    public static void main(String args[]){
        SpringApplication.run(DiscoveryServerApplication.class);
    }

    /**
     * This method is used to generate WAR of application
     * <br> No to maintain any deployment descriptor file (web.xml)
     * @param builder
     * @return SpringApplicationBuilder
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(DiscoveryServerApplication.class);
    }
}
